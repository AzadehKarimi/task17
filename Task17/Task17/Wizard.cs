﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17
{
    public class Wizard : CharacterClass
    {
        public Wizard()
        { }
        public Wizard(string name, string gender) : base(name, gender)
        {
            this.Hp = 300;
            this.Energy = 500;
            this.ArmorRating = 30;
        }


        public override string Attack()
        {
            return ("Attack is done by Wizard");
        }

        public override string Move()
        {
            return ("Move done by Wizard");
        }

    }
}
