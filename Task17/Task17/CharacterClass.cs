﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17
{
    public class CharacterClass
    {
        private int hp;
        private int energy;
        private string name;
        private int armorRating;
        private string gender;


        public CharacterClass()
        { }
        public CharacterClass(string name, string gender)
        {
            this.name = name;
            this.gender = gender;

        }

        public int Hp { get => hp; set => hp = value; }
        public int Energy { get => energy; set => energy = value; }
        public string Name { get => name; set => name = value; }
        public int ArmorRating { get => armorRating; set => armorRating = value; }
        public string Gender { get => gender; set => gender = value; }

        public virtual string Attack()
        {
            return "attack is done";
        }
        public virtual string Move()
        {
            return "Moved!";

        }

    }

}
