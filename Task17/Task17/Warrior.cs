﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17
{
    public class Warrior : CharacterClass
    {
        public Warrior()
        { }
        public Warrior(string name, string gender) : base(name, gender)
        {
            this.Hp = 100;
            this.Energy = 200;
            this.ArmorRating = 50;
        }


        public override string Attack()
        {
            return ("Attack Done by  " + Name + " which is Warrior ");
        }

        public override string Move()
        {
            return ("Move Done by" + Name + " which is Warrior ");
        }

    }
}

